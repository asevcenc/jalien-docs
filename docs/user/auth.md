# JAliEn Authentication

JAliEn authentication mechanism is based on the AliEn user certificates. If you
already have a working AliEn setup most probably you will not experience any
issues with logging into JAliEn.

There are two options to connect to the Grid using JAliEn, using the full
certificate or using a JAliEn token. The full certificate comes with all the
privileges assigned to the user, is usually valid for a year, encrypted with
a password and can be used to request tokens. The JAliEn tokens are very similar
to the certificates, they are just files with credentials stored in your
temporary directory readable by your current user only. Tokens can authenticate 
you to the Grid just as certificates and entitle you to almost all of the 
privileges without asking for any password. However, tokens cannot be used to 
request new tokens, and they are valid for a much shorter period than the full
certificates. So every so often you will be prompted for the full Grid certificate
password again.

All JAliEn clients will first try to connect with a token, to avoid asking you
for the password. If a valid token isn’t available, then the full certificate is
going to be loaded. If a client has connected with a full certificate, a fresh
token will be fetched automatically. The JBox (the local background
authentication and caching service) prefers the full grid certificates, asks for
password to decrypt it and keeps fetching fresh tokens for you. The end result
in any case is that you will be asked to type in the password at most once per
day, when it’s really needed.
