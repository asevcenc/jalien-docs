## Network connectivity

`checkAddr` command will create a TCP socket to specified ip/port destination (or to defaults)
```
alien.py checkAddr -h
checkAddr [reference] fqdn/ip port
defaults are: alice-jcentral.cern.ch 8097
reference arg will check connection to google dns and www.cern.ch
```
* `reference` argument will add checking of google dns and www.cern.ch

## Network latency

`ping` will time the ping/pong dialogue of the websocket connection.  
this will contain network RTT and the server answer latency.
```
alien.py ping -h
ping <count>
where count is integer
```

