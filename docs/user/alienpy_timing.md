
For debuggin/benchmarking purposes one can measure the duration of some operations

!!! tip "Session connection time"
    `ALIENPY_TIMECONNECT=1 alien.py pwd`
    it will print the time needed to establish session

!!! tip "Command/Query time"
    `alien.py time pwd`
    it will print total time spent in sending command, receive answer, decode answer.

!!! tip "Server-side DB query timing"
    `alien.py "timing on; pwd"`
    it will print the time taken by each DB query

