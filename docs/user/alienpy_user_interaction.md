## Shell vs command line

* Shell mode will read and interpret the command and arguments as provided by the user
* Command line mode (`alien.py command arg1 arg2 ..`) will first be interpreted by the shell
    * Make sure to quote what is passed to alien.py: `alien.py ‘command arguments’`

## Features

* Command completion (with TAB) (command only)
* LFN completion: in special cases that are aware of lfn only arguments.
    * N.B. subject to network latencies, each completion is a network query
* Command history saved to `${HOME}/.alienpy_history`
* Default saving of last CWD
    * Global value (racy between multiple parallel sessions)
        * so, pay attention when using relative paths
    * Disabled by env var `ALIENPY_NO_CWD_RESTORE` set to any value
* Shell mode prompt customizations (date and/or local cwd):
    * In the current session with:
        * `prompt date`
        * `prompt pwd`
    * Global settings of: `ALIENPY_PROMPT_DATE` and `ALIENPY_PROMPT_CWD`
* Chaining of commands by separator `;`
* Piping the result of first AliEn-side command to the local shell.
    * the first pipe will pass the result of left side to the right-side that is executed within local shell
* Aliases definition in `${HOME}/.alienpy_aliases`
    * Format: `tag=cmd1;cmd2;cmd3`
    * no logical operations between commands.
* Enable json output
    * per session: `alien.py -json command`
    * per command: `alien.py command -json arg1 arg2 ..`

## HELP

* Either `help` or just `?`; it will list all known commands
* Optionally suffixed by command; it will call the command help.
