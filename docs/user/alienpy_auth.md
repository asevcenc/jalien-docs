## Authentication/Authorization

Authentication is done via x509 SSL certificates 
When no token is available (missing or expired), the client will automatically create a token (AliEn side short lived certificate).

### Certificate/token locations

The location of user certificate can be customized by `X509_USER_{CERT,KEY}` env vars (should point to files),
and if not set the default location is `${HOME}/.globus/user{cert,key}.pem`

The location of token certificate can be customized by `JALIEN_TOKEN_{CERT,KEY}` env vars,
which can either point to a file, or have actually the certificate content (the client will create temporary files).
If these are not set, the default files `${TMPDIR}/token{cert,key}_$UID.pem` will be used. (if TMPDIR is not set, `/tmp` will be used)


!!! warning
    On clusters like lxplus `/tmp` is private to the node, so for each login the token will be lost.  
    Use/set a `TMPDIR` relative to the `${HOME}`


### Certificate/token information/verification

* Information on certificate/token is printed by commands: `cert-info`/`token-info`

* Verification against the _known_ CA certificates is done by : `cert-verify`/`token-verify`
!!! tip
    when certificate is known to be valid, this can check if the correct directory of CA certificates is seen.

* Checking the key against the certificate: `certkey-match`/`tokenkey-match`

!!! info
    These operations does not require network connectivity.


### Token operations

* alien-token-init is an alias to `alien.py token-init`
* `token-init` can be used for token customization
```
alien.py token-init -h
INFO: token is automatically created, use this for token customization
usage: token-init   [-options]

options:
-u <username>          :  switch to another role of yours
-v <validity (days)>   :  default depends on token type
-t <tokentype>         :  can be one of: job, jobagent, host, user (default)
-jobid <job DN extension>  :  expected to be present in a job token
-hostname <FQDN>       :  required for a host certificate
```
* `token-destroy ` will delete the token files.

!!! tip
    For programatic usage use `token` command (same options as `token-init`); print only command.  
    Allow usage like:
    ```
    eval $(alien.py -json token | jq -r '.["results"][0] |  "JALIEN_TOKEN_CERT=\"\(.tokencert)\"", "JALIEN_TOKEN_KEY=\"\(.tokenkey)\""')
    ```
    This will define in the current environment `JALIEN_TOKEN_CERT` and `JALIEN_TOKEN_KEY`


### Role change

`user <username>` command will change the role of current user temporary. (like doing `su <username>`)

!!! tip
    use `user` for temporary change of the role.  
    user `token-init` to create a new token, for prolonged use of the alternate user name.
