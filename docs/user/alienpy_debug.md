# alien.py Debugging

There are a few steps to follow

### Report the version
`alien.py version`

will provide an output like:
```
alien.py version: 1.3.7
alien.py version date: 20211202_153804
alien.py location: /cvmfs/alice.cern.ch/el7-x86_64/Packages/xjalienfs/1.3.7-19/lib/python3.6/site-packages/alienpy/alien.py
script location: /cvmfs/alice.cern.ch/el7-x86_64/Packages/xjalienfs/1.3.7-19/bin/alien.py
Interpreter: /cvmfs/alice.cern.ch/el7-x86_64/Packages/Python/v3.6.10-149/bin/python3.6
Python version: 3.6.10 (default, Mar 17 2022, 14:52:02) 
[GCC 10.2.0]
XRootD version: 5.4.2-alice1
XRootD path: /cvmfs/alice.cern.ch/el7-x86_64/Packages/XRootD/v5.4.2-alice1-3/lib/python/site-packages/XRootD/client/__init__.py
```

this is not network dependent so it can be executed at any time

### alien.py debug report

Enabling debug output  
`ALIENPY_DEBUG=1 ALIENPY_DEBUG_FILE=log.txt`   
    
* `ALIENPY_DEBUG` - will enable the debug messages   
* `ALIENPY_DEBUG_FILE` - allow specification of output logfile. in the example above `log.txt` will be placed in `CWD`   

### XRootD debug report

Enabling XRootD system log output
`XRD_LOGLEVEL='Dump' XRD_LOGFILE=xrdlog.txt`   

* `XRD_LOGLEVEL='Dump'` - will enable/change the log level; `Dump` is the highest
* `XRD_LOGFILE=xrdlog.txt` - will specify the location of the log file.

Additionally for checking the downloading with `xrdcp` one can enable   
the persistence of metafile(s) used for downloading:   
`ALIENPY_KEEP_META=1`

this will copy in the `CWD` the metafiles used for downloading, which can be used with xrdcp:   
`xrdcp -p -P -f -v meta_file_name.meta4 destination_file`

#### XRootD python import

For problems related to python loading of XRootD binding one can check with:   
`python3 -c 'from XRootD import client;print(client.__version__);'`

and if this is valid, the actual location of library can be checked with:   
`python3 -c 'from XRootD import client;print(client.__file__);'`

if the loading fails, one can ask on [alice-project-analysis-task-force@cern.ch](mailto:alice-project-analysis-task-force@cern.ch)   
or    
fill an issue on alidist repository [https://github.com/alisw/alidist/issues](https://github.com/alisw/alidist/issues)


### Developer contact
Adrian Sevcenco [adrian.sevcenco@cern.ch](mailto:adrian.sevcenco@cern.ch)
or
on mattermost `JAliEn` channel: [alice/channels/jalien](https://mattermost.web.cern.ch/alice/channels/jalien)   
or mattermost direct message: `@asevcenc`

There is also a possibility for discussions on [ALICE Talk](https://alice-talk.web.cern.ch/)   
but i have to be made aware of discussion so please mention me


